<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckUsuario;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::resource('/tweet', 'App\Http\Controllers\TweetController')->only(['store','index','show','destroy']);
Route::put('/tweet', 'App\Http\Controllers\TweetController@update');
Route::post('/tweet/{id}', 'App\Http\Controllers\TweetController@destroy');

Route::get('/user','App\Http\Controllers\UserController@index');
Route::put('/user','App\Http\Controllers\UserController@update')->middleware(CheckUsuario::class);
Route::delete('/user','App\Http\Controllers\UserController@destroy');

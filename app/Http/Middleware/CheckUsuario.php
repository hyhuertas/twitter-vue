<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckUsuario
{
    /**
     * Handle an incoming request.
     * @author Hector Huertas
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(isset($request->params['nombre']) && isset($request->params['id'])){
            return $next($request);
        }
    }
}

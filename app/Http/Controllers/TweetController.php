<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     * @author Hector Huertas
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario_id = auth()->user()->id;//trae el id del usuario logueado

        $tweeters = Tweet::where('user_id',$usuario_id)->with('user')->orderBy('id', 'DESC')->get();//me duevuelde de manera descendente los twites del usuario logueado

        $tweets = [];

        foreach ($tweeters as $key => $tweet) {
            $tweets['tweets'][$key] = $tweet;
            $tweets['tweets'][$key]['hora'] = $tweet->updated_at->format('Y-m-d');
        }

        return json_encode($tweets);
    }

    /**
     * Store a newly created resource in storage.
     * @author Hector Huertas
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $validate = $request->validate([
                'comentario' => 'required|max:140',//validaciones para el comentario que sea requerido y de maximo 140 caracteres
            ]);

            if ($request->comentario!=null) {
                $tweet = Tweet::create([
                    "content" => $request->comentario,
                    "user_id" => auth()->user()->id
                ]);
            }

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente',
            ];

            DB::commit();
            return $data_return;

        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;

        }
    }

    /**
     * Update the specified resource in storage.
     * @author Hector Huertas
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tweet $tweet)
    {
        DB::beginTransaction();
        try {

            $validate = $request->validate([
                'id_tweet' => 'required|numeric',//validacion del id para actualizar el tweet solicitado
                'comentario' => 'required|max:140'//validaciones para el comentario que sea requerido y de maximo 140 caracteres
            ]);


            if ($request->comentario!=null) {
                $editarTweet = Tweet::where('id', $request->id_tweet);

                $editarTweet = $editarTweet->update([
                    "content" => $request->comentario,
                    "user_id" => auth()->user()->id
                ]);
            }

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Tweet editado correctamente',
            ];

            DB::commit();
            return $data_return;

        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;

        }

    }

    /**
     * Remove the specified resource from storage.
     * @author Hector Huertas
     * @param  \App\Models\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            if ($id!=null) {
                $borrarTweet = Tweet::where('id', $id);

                $borrarTweet = $borrarTweet->delete();
            }

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Tweet borrado',
            ];

            DB::commit();
            return $data_return;

        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;

        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @author Hector Huertas
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::all();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     * Acualización de usuarios por medio del procedure update_usuarios
     * @author Hector Huertas
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $nombre = $request->params['nombre'];
        $id = (int)$request->params['id'];
        $update = User::where('id',$id)->update([
            'name'=>$nombre
        ]);
        if ($update) {
            return [
                'message' => 'Cuenta actualizada',
                'code' => 200,
                'data_erase?' => $update,
            ];
        }else{
            return [
                'message' => 'Cuenta no actualizada',
                'option' => 'revisa el codigo',
                'code' => 404,
                'data_erase?' => $update,
            ];
        }
        return $update;
    }

    /**
     * Remove the specified resource from storage.
     * Eliminación de la cuenta de usuario y a la vez eliminación de los registros hechos
     * @author Hector Huertas
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $borrar_twetts = Tweet::where('user_id',$request->id)->delete();
        $id_usuario = (int)$request->id;
        $delete = User::where('id',$id_usuario)->delete();
        if ($delete) {
            return [
                'message' => 'Cuenta borrada',
                'code' => 200,
                'data_erase?' => $delete,
            ];
        }else{
            return [
                'message' => 'Cuenta no borrada',
                'option' => 'revisa el codigo',
                'code' => 404,
                'data_erase?' => $delete,
            ];
        }
    }
}
